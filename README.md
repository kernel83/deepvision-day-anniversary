# DeepVision Day Anniversary

## Install it

**NOTICE**: this is "under construction" so you'll be installing a development environment.

### Requirements
* Git
* Docker and Docker Compose

### Steps
First, clone the repository:

```
git clone git@bitbucket.org:kernel83/deepvision-day-anniversary.git
```

Run the server:

```
cd deepvision-day-anniversary/
docker-compose up
```

You're good to go! Open your browser and enter http://127.0.0.1:8000/api/day_anniversaries/?date=2020-01-01 to verify the server is up and running. Of course, you'll see no anniversaries because the database is empty; you'll need to add some.

## Use it

Run the server:
```
cd deepvision-day-anniversary/
docker-compose up -d
```

To add anniversaries you'll need create a superuser first:
```
docker-compose exec web python manage.py createsuperuser
```

Go to http://127.0.0.1:8000/admin/ and log in using your superuser credentials. Hit "Day anniversaries" and add as many anniversaries as you want.

Since the API requires users to authenticate, go back to http://127.0.0.1:8000/admin/, hit "Tokens" and create a token for you. Then, you can access anniversaries via API (using curl):

```
curl -H 'Authorization: Token <YOUR_AUTH_TOKEN>' http://127.0.0.1:8000/api/day_anniversaries/?date=YYYY-MM-DD
```
(replace `<YOUR_AUTH_TOKEN>` with the token you've just created, and `YYYY-MM-DD` with a date of your choice)

### Try it out with Swagger
We include a UI (powered by Swagger) that allows you to _play_ with the API.

Go to http://127.0.0.1:8000/swagger/ and hit "Authorize". Enter "Token <YOUR_AUTH_TOKEN>" (replace `<YOUR_AUTH_TOKEN>` with your token), hit "Authorize" followed by "close".

Hit "day_anniversaries_list" > "try it out", enter an ISO-formatted date (YYYY-MM-DD) and execute. The response will show up below.

### Stop server
To stop the server:
```
docker-compose down
```

## Run tests

```
docker-compose run web python manage.py test
```
