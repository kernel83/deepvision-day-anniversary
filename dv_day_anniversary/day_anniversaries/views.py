from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import DayOfAnniversariesSerializer

PAGE_CACHE_TIMEOUT = getattr(settings,
                             'DAY_ANNIVERSARIES_PAGE_CACHE_TIMEOUT',
                             5 * 60)


class DayOfAnniversariesAPIView(APIView):
    permission_classes = [IsAuthenticated]

    @method_decorator(cache_page(PAGE_CACHE_TIMEOUT))
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                'date',
                openapi.IN_QUERY,
                description='ISO-formatted date (YYYY-MM-DD)',
                type=openapi.TYPE_STRING,
                format=openapi.FORMAT_DATE,
                required=True
            )
        ]
    )
    def get(self, request, format=None):
        """
        List day's anniversaries and anniversaries for the entire month
        """
        serializer = DayOfAnniversariesSerializer(data=request.query_params)
        # **NOTICE** there's no need to do `if serializer.is_valid(...)` since
        # we can use `is_valid` as a procedure to raise an exception if input
        # data is invalid.
        serializer.is_valid(raise_exception=True)

        # **NOTICE** this is just creating a instance of `DayOfAnniversaries`;
        # nothing is persisted in the DB.
        serializer.save()

        return Response(serializer.data)
