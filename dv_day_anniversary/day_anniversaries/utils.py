from calendar import monthrange

from .models import DayAnniversary


class DayOfAnniversaries:
    """
    Basically a collection of anniversaries (instances of `DayAnniversary`) for
    a particular date.
    """
    def __init__(self, date):
        self.date = date

    @property
    def anniversaries(self):
        return DayAnniversary.objects.filter(month=self.date.month,
                                             day=self.date.day)

    @property
    def titles(self):
        return list(self.anniversaries.values_list('title', flat=True))

    @property
    def month_anniversaries(self):
        return DayAnniversary.objects.filter(month=self.date.month)

    @property
    def month_titles(self):
        _, number_of_days = monthrange(self.date.year, self.date.month)
        month_titles = {
            month_day: []
            for month_day in range(1, number_of_days + 1)
        }
        for anniversary in self.month_anniversaries:
            month_titles[anniversary.day].append(anniversary.title)
        return month_titles
