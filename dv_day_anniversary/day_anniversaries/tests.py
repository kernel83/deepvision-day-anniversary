from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from .recipes import day_anniversary_recipe


class DayOfAnniversariesAPITestCase(APITestCase):
    """
    A set of tests for `DayOfAnniversariesAPIView`
    """
    def setUp(self):
        self.user = User.objects.create(username='alice')

        self.javi_anniversary = day_anniversary_recipe.make(
            month=6,
            day=19,
            title="Javi's Birthday"
        )
        self.unc_anniversary = day_anniversary_recipe.make(
            month=6,
            day=19,
            title="UNC's anniversary"
        )
        self.mason_anniversary = day_anniversary_recipe.make(
            month=6,
            day=24,
            title="Day of Masons"
        )
        self.archer_anniversary = day_anniversary_recipe.make(
            month=8,
            day=1,
            title="Day of Archers"
        )
        self.lapwing_anniversary = day_anniversary_recipe.make(
            month=8,
            day=21,
            title="Day of Lapwings"
        )

    def test_first_day_of_june(self):
        """
        Ensure we get the anniversaries for June 1th and other anniversaries
        celebrated in June.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get(
            reverse('day-anniversaries'),
            {'date': '2018-6-1'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data.get('today'), [])

        self.assertEqual(
            set(response.data.get('month').get(19)),
            {self.javi_anniversary.title, self.unc_anniversary.title}
        )
        self.assertEqual(response.data.get('month').get(24),
                         [self.mason_anniversary.title])

    def test_first_day_of_august(self):
        """
        Ensure we get the anniversaries for August 1th and other anniversaries
        celebrated in August.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get(
            reverse('day-anniversaries'),
            {'date': '2018-8-1'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data.get('today'),
                         [self.archer_anniversary.title])

        self.assertEqual(response.data.get('month').get(10), [])
        self.assertEqual(response.data.get('month').get(21),
                         [self.lapwing_anniversary.title])

    def test_invalid_date(self):
        """
        Make sure we get a 400 if the date is invalid.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get(
            reverse('day-anniversaries'),
            {'date': '2020-2-30'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.get('date')[0].code, 'invalid')

    def test_missing_date(self):
        """
        Make sure we get a 400 if the date is missing.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get(reverse('day-anniversaries'), format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.get('date')[0].code, 'required')

    def test_unknown_param(self):
        """
        Make sure we get a 400 if we send an unrecognized parameter.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get(
            reverse('day-anniversaries'),
            {'date': '2018-6-1', '__unknown_param__': '2020-2-30'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.get('non_field_errors')[0].code,
                         'invalid')

    def test_wrong_methods(self):
        """
        Make sure some specific methods are not allowed
        """
        self.client.force_authenticate(user=self.user)
        wrong_methods = (self.client.post,
                         self.client.put,
                         self.client.patch,
                         self.client.delete)
        for wrong_method in wrong_methods:
            response = wrong_method(
                reverse('day-anniversaries'),
                {'foo': 'bar'},
                format='json'
            )
            self.assertEqual(response.status_code,
                             status.HTTP_405_METHOD_NOT_ALLOWED)
            self.assertEqual(response.data.get('detail').code,
                             'method_not_allowed')

    def test_not_authenticated_user(self):
        """
        Ensure that not authenticated users are not allowed to use the service
        """
        response = self.client.get(
            reverse('day-anniversaries'),
            {'date': '2018-6-1'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data.get('detail').code, 'not_authenticated')
