from model_mommy.recipe import Recipe

from .models import DayAnniversary


day_anniversary_recipe = Recipe(DayAnniversary)
