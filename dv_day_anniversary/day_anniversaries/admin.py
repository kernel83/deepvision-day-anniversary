from calendar import monthrange

from django import forms
from django.contrib import admin

from .models import DayAnniversary


class DayAnniversaryAdminForm(forms.ModelForm):
    class Meta:
        model = DayAnniversary
        fields = '__all__'

    def clean(self):
        month = self.cleaned_data.get('month')
        day = self.cleaned_data.get('day')
        if not (1 <= month <= 12) or not 1 <= day <= monthrange(0, month)[1]:
            raise forms.ValidationError('Date is invalid')
        return self.cleaned_data


class DayAnniversaryAdmin(admin.ModelAdmin):
    form = DayAnniversaryAdminForm


admin.site.register(DayAnniversary, DayAnniversaryAdmin)
