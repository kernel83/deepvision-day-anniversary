from django.apps import AppConfig


class DayAnniversariesConfig(AppConfig):
    name = 'day_anniversaries'
