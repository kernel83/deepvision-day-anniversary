from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .utils import DayOfAnniversaries


class DayOfAnniversariesSerializer(serializers.Serializer):
    """
    A simple serializer for instances of `DayOfAnniversary`
    """
    date = serializers.DateField(write_only=True)
    today = serializers.SerializerMethodField(method_name='get_titles')
    month = serializers.SerializerMethodField(method_name='get_month_titles')

    def create(self, validated_data):
        return DayOfAnniversaries(**validated_data)

    def validate(self, attrs):
        # **NOTICE** this might not be nice. As far as I know, a common
        # practice is to allow unknown fields.
        unknown_field_names = set(self.initial_data) - set(self.fields)
        if unknown_field_names:
            raise ValidationError(
                "Unknown field(s): {}".format(", ".join(unknown_field_names))
            )
        return attrs

    def get_titles(self, day):
        return day.titles

    def get_month_titles(self, day):
        return day.month_titles
