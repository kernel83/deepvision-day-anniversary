from django.conf.urls import url

from .views import DayOfAnniversariesAPIView

urlpatterns = [
    url(
        r'^day_anniversaries/$',
        DayOfAnniversariesAPIView.as_view(),
        name='day-anniversaries'
    ),
]
