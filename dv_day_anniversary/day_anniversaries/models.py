from django.db import models


class DayAnniversary(models.Model):
    """
    A model for day anniversaries
    """
    month = models.PositiveIntegerField()
    day = models.PositiveIntegerField()
    title = models.CharField(max_length=128)

    class Meta:
        verbose_name_plural = 'Day anniversaries'

    def __repr__(self):
        return '{}(month={}, day={}, title={})'.format(
            type(self).__name__,
            self.month,
            self.day,
            repr(self.title)
        )

    def __str__(self):
        return repr(self)
